﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BowlingBall.Tests
{
    [TestClass]
    public class GameFixture
    {
        Game game;
        public GameFixture()
        {
            game = new Game(new Spare(), new Strike());
        }
        [TestMethod]
        public void Gutter_game_score_should_be_zero_test()
        {
            Roll(0, 20);
            Assert.AreEqual(0, game.GetScore());
        }

        [TestMethod]
        public void TestPerfectGame()
        {
            Roll(10, 12);
            Assert.AreEqual(300, game.GetScore());
        }

        [TestMethod]
        public void TestAllOnes()
        {
            Roll(1, 20);
            Assert.AreEqual(20, game.GetScore());
        }

        [TestMethod]
        public void TestOneSpareAndOneStrikes()
        {
            game.Roll(8);
            game.Roll(2);

            game.Roll(10);

            Roll(0, 17);
            Assert.AreEqual(30, game.GetScore());
        }

        [TestMethod]
        public void TestRandomGameWithSpareThenStrikeAtEnd()
        {
            game.Roll(10);

            game.Roll(9);
            game.Roll(1);

            game.Roll(5);
            game.Roll(5);

            game.Roll(7);
            game.Roll(2);

            game.Roll(10);

            game.Roll(10);

            game.Roll(10);

            game.Roll(9);
            game.Roll(0);

            game.Roll(8);
            game.Roll(2);

            game.Roll(9);
            game.Roll(1);
            game.Roll(10);

            Assert.AreEqual(187, game.GetScore());
        }

        private void Roll(int pins, int times)
        {
            for (int i = 0; i < times; i++)
            {
                game.Roll(pins);
            }
        }
    }
}
