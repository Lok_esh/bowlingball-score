﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingBall.Interfaces
{
    public interface IStrike
    {
        bool isStrike(int frameIndex,int[] rolls);
    }
}
