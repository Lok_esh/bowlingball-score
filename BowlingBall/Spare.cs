﻿using BowlingBall.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingBall
{
    public class Spare : ISpare
    {
        public bool isSpare(int frameIndex, int[] rolls)
        {
            try
            {
                return rolls[frameIndex] + rolls[frameIndex + 1] == 10;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
