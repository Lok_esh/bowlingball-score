﻿using BowlingBall.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingBall
{
    public class Game : IGame
    {
        private int[] rolls = new int[21];
        private int[] frame = new int[10];
        int currentRoll = 0;

        ISpare _spare;
        IStrike _strike;

        public Game(ISpare spare, IStrike strike)
        {
            _spare = spare;
            _strike = strike;
        }
        public void Roll(int pins)
        {
            // Add your logic here. Add classes as needed.
            try
            {
                rolls[currentRoll++] = pins;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public int GetScore()
        {
            // Returns the final score of the game.
            int score = 0;
            int frameIndex = 0;
            try
            {
                for (int frame = 0; frame < 10; frame++)
                {
                    if (_strike.isStrike(frameIndex, rolls))
                    {
                        score += 10 + rolls[frameIndex + 1] + rolls[frameIndex + 2];
                        frameIndex++;
                    }

                    else if (_spare.isSpare(frameIndex, rolls))
                    {
                        score += 10 + rolls[frameIndex + 2];
                        frameIndex += 2;
                    }
                    else
                    {
                        score += rolls[frameIndex] + rolls[frameIndex + 1];
                        frameIndex += 2;
                    }

                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return score;
            
        }

    }
}
